Calendar Plugin
===============

Embedded calendar view for Kanboard.

Fixes:

- Ability to resize started tasks in the calendar week view
- Ability to move task with any due date (including ones with due time set) in any calendar view
- Week starts with monday, instead of sunday

Author
------

- Frédéric Guillot
- Andrew Cziryak (Maintainer)
- License MIT

Requirements
------------

- Kanboard >= 1.2.13

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/Calendar`
3. Clone this repository into the folder `plugins/Calendar`

Note: Plugin folder is case-sensitive.
